# Pure C++ TLS-encrypted chat 

 - This chat is intended to be written on pure C++ using [Botan](https://botan.randombit.net/) as a TLS encryption library, POSIX sockets (for UNIX 3 and 4) and wXwidgets as a graphics library. 
