#ifndef __SOCKETS_HPP__
#define __SOCKETS_HPP__

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <string>
#include <sys/uio.h>
#include <stdexcept>



// This is my RAII wrapper for handling sockets
// TODO:
/*
 * Make connect, read and other useful functions
 * Create exception management
 */

class SocketBase
{
	private:
		int socketDescriptor;
	public:
		virtual ~SocketBase();

		// AFAIK sockets are not supposed to be copyable but movable instead
		
		SocketBase(SocketBase const&) = delete;
		SocketBase& operator=(SocketBase const&) = delete;
		

		SocketBase(SocketBase&& otherSocket) noexcept;
		SocketBase& operator=(SocketBase&& otherSocket) noexcept;
		void swap(SocketBase& otherSocket) noexcept;
		
		void close();
	protected:
		static constexpr int invalidSocketDescriptor = -1;
		SocketBase(int socketDescriptor);
		int getSocketDescriptor() const noexcept {return socketDescriptor;};
};

class TCPSocket : public SocketBase
{
	public:
		TCPSocket(int socketDescriptor) : SocketBase{socketDescriptor} {}
		
		template<typename T>
		size_t recieveMessage(char* buffer, size_t size, T scanForEnd = 
				[](size_t){return false;}){
					if (getSocketDescriptor() == 0)
					{
						throw std::logic_error(
							buildErrorMessage(
								"TCPSocket::",
								__func__, 
								" : accept called on a bad socket",
								strerror(errno)
							)
						);
					}

					size_t dataRead = 0;

					while (dataRead < size)
					{
						size_t recvd = read(getSocketDescriptor(),
							buffer + dataRead,
							size - dataRead);
						if (recvd == static_cast<size_t>(-1))
						{
							switch (errno)
							{
								case ENXIO:
								{
									throw std::domain_error(buildErrorMessage("TCPSocket::", __func__, ": read: critical error: ", strerror(errno)));
									break;
								}
								case EAGAIN:{
									continue;
								}
								case ENOTCONN:{
									recvd = 0;
									break;
								}
								default:{
									throw std::runtime_error(buildErrorMessage("TCPSocket::", __func__, ": read: returned -1: ", strerror(errno)));
									break;
								}
							}
						}
						if (recvd == 0)
						{
							break;
						}
						dataRead += recvd;
						
						if (scanForEnd(dataRead))
						{
							break;
						}
					}
					return dataRead;
				}
		void sendMessage(char const * buffer, size_t size);
		void putMessageClose();
};

class RemoteConnectSocket : public TCPSocket 
{
	public:
		RemoteConnectSocket(std::string const& host, size_t port);
};

class ServerSocket : public SocketBase
{
	public:
		ServerSocket(size_t port);
		TCPSocket accept();
	private:
		static constexpr int maxConnectionBacklog = 10;

};



#endif
