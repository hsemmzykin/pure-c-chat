#ifndef __CLIENT_WINDOW_HPP__
#define __CLIENT_WINDOW_HPP__
#include <wx/wxprec.h>
#include <wx/wx.h>

#include <wx/frame.h>
#include <wx/textctrl.h>

class MainApp: public wxApp 
{
    public: 
    virtual bool OnInit();  
};

class MainFrame: public wxFrame 
{ 
    public:
        MainFrame( const wxString& title, const wxPoint& pos, const wxSize& size );
        wxTextCtrl *MainEditBox;
        wxMenuBar *MainMenu;
        void Quit(wxCommandEvent& event);
        void NewFile(wxCommandEvent& event);
        void OpenFile(wxCommandEvent& event);
        void SaveFile(wxCommandEvent& event);
        void SaveFileAs(wxCommandEvent& event);
        void CloseFile(wxCommandEvent& event);

    DECLARE_EVENT_TABLE()
};

enum
{
    TEXT_Main = wxID_HIGHEST + 1, // declares an id which will be used to call our button
    MENU_New,
    MENU_Open,
    MENU_Close,
    MENU_Save,
    MENU_SaveAs,
    MENU_Quit
};

#endif