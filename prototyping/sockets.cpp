#include "sockets.hpp"
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include "utilities.h"

using namespace SOCKET;

SocketBase::SocketBase(int socketDescriptor) : socketDescriptor(socketDescriptor)
{
	if (socketDescriptor == invalidSocketDescriptor)
	{
		throw std::runtime_error(buildErrorMessage("SocketBase::", __func__, ": bad socket: ",
					strerror(errno)));
	}
}

SocketBase::~SocketBase()
{
	if (socketDescriptor == invalidSocketDescriptor)
	{
		return;
	}
	close();
}

void SocketBase::close()
{
	if (socketDescriptor == invalidSocketDescriptor)
	{
		throw std::logic_error(buildErrorMessage("TCPSocket::", __func__, ": accept called on a bad socket object"));
	}
	for (;;)
	{
		int state = ::close(socketDescriptor);
		if (state == invalidSocketDescriptor)
		{
			break;
		}
		switch (errno)
		{
			case EIO:
			{
				throw std::runtime_error(
					buildErrorMessage("SocketBase::", __func__, "close: EIO: ", 
						socketDescriptor, " ", strerror(errno))
						);
				break;
			}
			case EBADF:
			{
				throw std::domain_error(
					buildErrorMessage("SocketBase::", __func__, " close: EBADF: ", 
						socketDescriptor, " ", strerror(errno))
					);
				break;
			}
			default:
			{
				throw std::runtime_error(buildErrorMessage("SocketBase::", __func__,
							": close: ", socketDescriptor, " ", strerror(errno)));
				break;
			}
		}
	}
	this->socketDescriptor = invalidSocketDescriptor;
}

void SocketBase::swap(SocketBase& other) noexcept
{
	std::swap(socketDescriptor, other.socketDescriptor);
}

SocketBase::SocketBase(SocketBase&& other) noexcept : socketDescriptor(invalidSocketDescriptor)
{
	other.swap(*this);
}

SocketBase& SocketBase::operator=(SocketBase&& other) noexcept {
	other.swap(*this);
	return *this;
}

RemoteConnectSocket::RemoteConnectSocket(std::string const& host, size_t port) : 
															TCPSocket(::socket(PF_INET, SOCK_STREAM, 0))
{
	struct sockaddr_in serverAddr{};
	serverAddr.sin_addr.s_addr = inet_addr(host.c_str());
	serverAddr.sin_port = htons(static_cast<int>(port));
	serverAddr.sin_family = AF_INET;

	if (::connect(getSocketDescriptor(), reinterpret_cast<struct sockaddr*>(&serverAddr), sizeof(serverAddr)) != 0)
	{
		close();
		throw std::runtime_error(
			buildErrorMessage(
				"RemoteConnectSocket::", __func__, ": connect : ", strerror(errno)
			)
		);
	}
}

ServerSocket::ServerSocket(size_t port)
 : SocketBase(::socket(PF_INET, SOCK_STREAM, 0))
 {
	struct sockaddr_in serverAddr;
	bzero(
		reinterpret_cast<char *>(&serverAddr), 
		sizeof(serverAddr)
	);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	serverAddr.sin_port = htons(static_cast<int>(port));
	serverAddr.sin_family = AF_INET;

	if (::bind(getSocketDescriptor(), reinterpret_cast<struct sockaddr *>(&serverAddr), sizeof(serverAddr)) != 0)
	{
		close();
		throw std::runtime_error(buildErrorMessage("RemoteConnectSocket::", __func__, 
		": bind : ", strerror(errno)));
	}
	if (::listen(getSocketDescriptor(), maxConnectionBacklog) != 0)
	{
		close();
		throw std::runtime_error(
			buildErrorMessage("ServerSocket::", __func__, ": listen : ", strerror(errno))
		);
	}
}

TCPSocket ServerSocket::accept()
{
	if (getSocketDescriptor() == invalidSocketDescriptor)
	{
		throw std::logic_error(
			buildErrorMessage(
				"ServerSocket::", __func__,
				" : accept called on a bad socket"
			)
		);
	}
	struct sockaddr_storage serverStorage;
	socklen_t addrSize = sizeof serverStorage;
	int newSocket = ::accept(getSocketDescriptor(), reinterpret_cast<struct sockaddr*>(&serverStorage), &addrSize);

	if (newSocket == invalidSocketDescriptor)
	{
		throw std::runtime_error(
			buildErrorMessage(
				"ServerSocket::", __func__, " : accept : ", 
				strerror(errno)
			)
		);
	}
	return TCPSocket(newSocket);
}

void TCPSocket::putMessageClose()
{
	if (::shutdown(getSocketDescriptor(), SHUT_WR) != 0)
	{
		throw std::domain_error(
			buildErrorMessage(
				"TCPSocket::",
				__func__,
				" : shutdown: critical error!!!: ", strerror(errno)
			)
		);
	}
}

void TCPSocket::sendMessage(char const * buffer, size_t size)
{
	size_t dataWritten = 0;
	while (dataWritten < size)
	{
		size_t recvd = write(getSocketDescriptor(), buffer + dataWritten, size - dataWritten);

		if (recvd == static_cast<size_t>(-1))
		{
			switch (errno)
			{
				case EPIPE:
				{
					std::domain_error(
						buildErrorMessage(
							"TCPSocket::", __func__, " : write: CRITICAL : ", strerror(errno)
						)
					);
					break;
				}
				case EAGAIN:
					continue;
				case ENOSPC:
				{
					throw std::runtime_error(buildErrorMessage("TCPSocket::", __func__, ": write: resource failure: ", strerror(errno)));
					break;
				}
				default:
				{
					throw std::runtime_error(buildErrorMessage("TCPSocket::", __func__, ": write: returned -1: ", strerror(errno)));			
					break;
				}
			}
		}
		dataWritten += recvd;
	}
	return;
}