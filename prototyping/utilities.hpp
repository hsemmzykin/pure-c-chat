#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include <string>
#include <sstream>
#include <utility>
#include <cstddef>

namespace SOCKET {
	
	template<typename... Args>
	int print(std::ostream& s, Args&... args)
	{
		using Expander = int[];
		return Expander {0, ((s << std::forward<Args>(args)), 0)...}[0];
	}

	template<typename... Args>
	std::string buildStringFromParts(Args const&... args)
	{
		std::stringstream message;
		print(message, args);
		return message.str();
	}

	template<typename... Args>
	std::string buildErrorMessage(Args const&... args)
	{
		return buildStringFromParts(args...);
	}
}
#endif
		
